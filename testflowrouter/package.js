Package.describe({
  name: 'testflowrouter',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/MayadaMuhammad/flowRouterTestingPackage',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.3.2');

  api.use('ecmascript');
  api.use('kadira:flow-router');
  api.use('http');
  api.use('session');
  api.mainModule('testflowrouter.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('testflowrouter');
  api.mainModule('testflowrouter-tests.js');
});
