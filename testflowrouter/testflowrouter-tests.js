// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";
import { FlowRouter } from "meteor/kadira:flow-router";
import { HTTP } from 'meteor/http';
import { Session } from 'meteor/session'

// Import and rename a variable exported by testflowrouter.js.
import { name as packageName } from "meteor/testflowrouter";


if(Meteor.isServer) {
	var adminRoutes = FlowRouter.group({
		prefix: '/admin',
		name: 'admin',
		triggersEnter: [function(context, redirect) {
			console.log('running group triggers');
		}]
	});

	adminRoutes.route('/posts', {
		action: function() {
			console.log("wazzap");
		},
		triggersEnter: [function(context, redirect) {
			console.log('running /admin trigger');
		}]
	});

	Meteor.methods({
		'testRoute': function() {
			try {
				var result = HTTP.call("GET", "http://localhost:3000/admin/posts");
				console.log("bang");
				console.log(result.statusCode);
			} catch (e) {
				console.log(e);
				throw new Meteor.Error(e);
			}
		}
	});
}

if(Meteor.isClient) {


	Tinytest.add('testflowrouter - example', function (test) {	
	  test.equal(packageName, "testflowrouter");
	});

	Tinytest.add('testflowrouter - example2', function (test) {

	  /*Meteor.call('testRoute', function(error, reponse) {
	  	if(error) {	
	  		Session.set("success", 0);  		
	  	}
	  	else {
	  		Session.set("success", 1);
	  	}
	  });*/

	  //test.equal(Session.get("success"), 1);
	  test.throws(Meteor.call('testRoute'));
	});
}

